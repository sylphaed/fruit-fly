<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<html>
<head>
</head>
<body>
<h1><bean:write name="postForm" property="title" /></h1>
<hr />
<h2><bean:write name="postForm" property="author" /></h2>

<p><bean:write name="postForm" property="body" /></p>
</body>
</html>