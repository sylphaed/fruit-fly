<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<html>
<head>
</head>
<body>
	<logic:iterate name="indexForm" property="postList" id="postList" >
		<p>
			<bean:write name="postList" property="title" />
			by <bean:write name="postList" property="author" />
		</p>
	</logic:iterate>
</body>
</html>