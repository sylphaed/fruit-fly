package net.riftwalkinto1v3;

import org.apache.commons.validator.routines.EmailValidator;
import java.util.Date;


public class User {

	public User(String name, String pw, String email) {
		//TODO name validation, email uniqueness check
		this.name = name;
		this.pw = pw;
		this.email = email;
		this.timeCreated = new Date();
		this.canPost = false;
		this.canComment = false;
		this.recoveryQuestion = "A recovery question has not been set.";
		this.recoveryAnswer = "";
		this.birthday = null;
		this.emailVerified = false;
	}
		
	private String name;
	private String pw;
	private String email;
	private Date timeCreated;
	private Date birthday;
	private String recoveryQuestion;
	private String recoveryAnswer;
	private boolean canPost;
	private boolean canComment;
	private boolean emailVerified;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getRecoveryQuestion() {
		return recoveryQuestion;
	}
	public void setRecoveryQuestion(String recoveryQuestion) {
		this.recoveryQuestion = recoveryQuestion;
	}
	public String getRecoveryAnswer() {
		return recoveryAnswer;
	}
	public void setRecoveryAnswer(String recoveryAnswer) {
		this.recoveryAnswer = recoveryAnswer;
	}
	public boolean canPost() {
		return canPost;
	}
	public void setCanPost(boolean canPost) {
		this.canPost = canPost;
	}
	public boolean canComment() {
		return canComment;
	}
	public void setCanComment(boolean canComment) {
		this.canComment = canComment;
	}
	public static boolean emailIsValid(String email){
		EmailValidator ev = EmailValidator.getInstance();
		return ev.isValid(email);
	}
	public boolean emailIsValid(){
		EmailValidator ev = EmailValidator.getInstance();
		return ev.isValid(this.email);
	}
	public boolean isEmailVerified() {
		return emailVerified;
	}
	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}
	
}
