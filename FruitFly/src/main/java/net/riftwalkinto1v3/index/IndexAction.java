package net.riftwalkinto1v3.index;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.riftwalkinto1v3.PostDAO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class IndexAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try (Connection connection = DriverManager
				.getConnection("jdbc:sqlite:testdb.db");) {

			PostDAO postDAO = new PostDAO(connection);

			IndexForm indexForm = (IndexForm) form;
			
			indexForm.setPostList(postDAO.getAllPosts());

			return mapping.findForward("success");
		}
	}
}
