package net.riftwalkinto1v3.index;

import java.util.ArrayList;

import net.riftwalkinto1v3.Post;

import org.apache.struts.action.ActionForm;

public class IndexForm extends ActionForm {

	private static final long serialVersionUID = 2472061606223884981L;
	private ArrayList<Post> postList;
	
	public ArrayList<Post> getPostList() {
		return postList;
	}
	public void setPostList(ArrayList<Post> postList) {
		this.postList = postList;
	}

}
