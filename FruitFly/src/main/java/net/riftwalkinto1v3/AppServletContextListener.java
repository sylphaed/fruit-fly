package net.riftwalkinto1v3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class AppServletContextListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent arg0) {
		try {
			java.sql.Driver sqliteDriver = DriverManager
					.getDriver("jdbc:sqlite:testdb.db");
			DriverManager.deregisterDriver(sqliteDriver);
		} catch (SQLException e) {
			throw new RuntimeException();
		}
	}

	public void contextInitialized(ServletContextEvent arg0) {
		setUpDB();
	}

	private void setUpDB() {
		try {
			Class.forName("org.sqlite.JDBC");
			try (Connection connection = DriverManager
					.getConnection("jdbc:sqlite:testdb.db");
					Statement statement = connection.createStatement();) {
				statement
						.executeUpdate("CREATE TABLE IF NOT EXISTS USERS("
								+ "NAME  			CHAR		NOT NULL CHECK (LENGTH(NAME) BETWEEN 4 AND 25), "
								+ "PASSWORD			CHAR		NOT NULL CHECK (LENGTH(PASSWORD)>=6), "
								+ "EMAIL			CHAR		NOT NULL, "
								+ "TIMECREATED		DATETIME	NOT NULL, "
								+ "BIRTHDAY			DATE, "
								+ "RECOVERYQUESTION	CHAR, "
								+ "RECOVERYANSWER	CHAR, "
								+ "CANPOST			BOOLEAN		NOT NULL, "
								+ "CANCOMMENT		BOOLEAN		NOT NULL, "
								+ "EMAILVERIFIED	BOOLEAN		NOT NULL, "
								+ "PRIMARY KEY(NAME, EMAIL))");

				statement.executeUpdate("CREATE TABLE IF NOT EXISTS POSTS("
						+ "DATE	DATETIME PRIMARY KEY	NOT NULL, "
						+ "AUTHOR			CHAR		NOT NULL, "
						+ "TITLE			CHAR		NOT NULL, "
						+ "BODY				TEXT		NOT NULL, "
						+ "FOREIGN KEY (AUTHOR) REFERENCES USERS(NAME))");

				statement.executeUpdate("CREATE TABLE IF NOT EXISTS COMMENTS("
						+ "DATE 			DATETIME	NOT NULL, "
						+ "POSTKEY			DATETIME	NOT NULL, "
						+ "AUTHOR			CHAR		NOT NULL, "
						+ "BODY				TEXT		NOT NULL, "
						+ "FOREIGN KEY (POSTKEY) REFERENCES POSTS(DATE),"
						+ "FOREIGN KEY (AUTHOR) REFERENCES USERS(NAME))");

			} catch (SQLException e) {
				throw new RuntimeException();
			}
		} catch (ClassNotFoundException e) {
			throw new RuntimeException();
		}
	}

}
