package net.riftwalkinto1v3;

import java.util.Date;

public class Comment {

	public Comment(Date date, Date postKey, String author, String body) {
		this.date = date;
		this.postKey = postKey;
		this.author = author;
		this.body = body;
	}
	
	private Date date;
	private Date postKey;
	private String author;
	private String body;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getPostKey() {
		return postKey;
	}
	public void setPostKey(Date postKey) {
		this.postKey = postKey;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}

}
