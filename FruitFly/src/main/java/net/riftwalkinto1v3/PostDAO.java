package net.riftwalkinto1v3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

public class PostDAO {

	public PostDAO(Connection connection) {
		this.connection = connection;
	}

	private final Connection connection;

	public void storePost(Post post) throws SQLException {
		try (Statement statement = connection.createStatement();
				PreparedStatement ps = connection.prepareStatement("INSERT "
						+ "INTO POSTS VALUES(?, ?, ?, ?)");) {
			ps.setDate(1, new java.sql.Date(post.getDate().getTime()));
			ps.setString(2, post.getAuthor());
			ps.setString(3, post.getTitle());
			ps.setString(4, post.getBody());
			ps.executeUpdate();
		}
	}
	
	public Post getNewestPost() throws SQLException {
		Post p = null;
		try (Statement statement = connection.createStatement();
				PreparedStatement ps = connection.prepareStatement("SELECT * "
						+ "FROM POSTS WHERE DATE = (SELECT MAX(DATE) FROM POSTS)")){
					try(ResultSet rs = ps.executeQuery()){
						if (rs.next())
							p = buildPost(rs);
						else{
							//TODO if a post search comes up empty
						}
					}
				}
		
		return p;
	}
	
	public ArrayList<Post> getAllPosts() throws SQLException{
		ArrayList<Post> list = new ArrayList<>();
		try(Statement statement = connection.createStatement();
				PreparedStatement ps = connection.prepareStatement("SELECT *"
						+ "FROM POSTS ORDER BY DATE DESC")){
			try(ResultSet rs = ps.executeQuery()){
				while (rs.next())
					list.add(buildPost(rs));
			}
			
		}
		
		return list;
	}
	
	public Post getPostByDate(Date date) throws SQLException {
		Post p = null;
		try (Statement statement = connection.createStatement();
				PreparedStatement ps = connection
						.prepareStatement("SELECT * FROM POSTS WHERE DATE = ?");
			) {
			ps.setDate(1, new java.sql.Date(date.getTime()));
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next())
					p = buildPost(rs);
				else{
					//TODO if a post search comes up empty
				}
					
			}
		}
		return p;
	}
	
	private Post buildPost(ResultSet rs) throws SQLException{
		return new Post(rs.getDate("DATE"), rs.getString("AUTHOR"), 
				rs.getString("TITLE"), rs.getString("BODY"));
	}
}
