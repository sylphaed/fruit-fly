package net.riftwalkinto1v3.post;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.riftwalkinto1v3.Post;
import net.riftwalkinto1v3.PostDAO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PostAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try (Connection connection = DriverManager
				.getConnection("jdbc:sqlite:testdb.db");) {

			PostDAO postDAO = new PostDAO(connection);
			Post post = postDAO.getNewestPost();

			PostForm postForm = (PostForm) form;

			postForm.setTitle(post.getTitle());
			postForm.setDate(post.getDate());
			postForm.setAuthor(post.getAuthor());
			postForm.setBody(post.getBody());

			return mapping.findForward("success");
		}
	}

}
