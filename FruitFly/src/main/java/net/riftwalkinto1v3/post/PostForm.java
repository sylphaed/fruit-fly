package net.riftwalkinto1v3.post;

import java.util.ArrayList;
import java.util.Date;

import net.riftwalkinto1v3.Comment;

import org.apache.struts.action.ActionForm;

public class PostForm extends ActionForm {

	private static final long serialVersionUID = -6527244748495826435L;
	private Date date;
	private String author;
	private String title;
	private String body;
	private ArrayList<Comment> commentList;
	
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public ArrayList<Comment> getCommentList() {
		return commentList;
	}
	public void setCommentList(ArrayList<Comment> commentList) {
		this.commentList = commentList;
	}

}
