package net.riftwalkinto1v3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDAO {

	public UserDAO(Connection connection) {
		this.connection = connection;		
	}
	
	private final Connection connection;

	public void storeUser(User u) throws SQLException{
		try (
				Statement statement = connection.createStatement();
				PreparedStatement ps = connection.prepareStatement("INSERT "
						+ "INTO USERS VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			) {
			ps.setString(1, u.getName());
			ps.setString(2, u.getPw());
			ps.setString(3, u.getEmail());
			ps.setDate(4, new java.sql.Date(u.getTimeCreated().getTime()));
			ps.setDate(5, new java.sql.Date(u.getBirthday().getTime()));
			ps.setString(6, u.getRecoveryQuestion());
			ps.setString(7, u.getRecoveryAnswer());
			ps.setBoolean(8, u.canPost());
			ps.setBoolean(9, u.canComment());
			ps.setBoolean(10, u.isEmailVerified());
			ps.executeUpdate();
		}
	}
	


	public User getUserByEmail(String email) throws SQLException {
		User u = null;
		try (Statement statement = connection.createStatement();
				PreparedStatement ps = connection
						.prepareStatement("SELECT * FROM USERS WHERE EMAIL = ?");) {
			ps.setString(1, email);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next())
					u = buildUser(rs);
				else {
					//TODO if a user search comes up empty
				}
			}
		}
		return u;
	}

	public User getUserByName(String name) throws SQLException {
		User u = null;
		try (Statement statement = connection.createStatement();
				PreparedStatement ps = connection
						.prepareStatement("SELECT * FROM USERS WHERE NAME = ?");) {
			ps.setString(1, name);
			try (ResultSet rs = ps.executeQuery()){
				if (rs.next())
					u = buildUser(rs);
				else {
					//TODO if a user search comes up empty
				}
			}
		}
		return u;

	}
	
	private User buildUser(ResultSet rs) throws SQLException{
		User u = new User(rs.getString("NAME"), rs.getString("PASSWORD"), 
				rs.getString("EMAIL"));
		u.setTimeCreated(rs.getDate("TIMECREATED"));
		u.setBirthday(rs.getDate("BIRTHDAY"));
		u.setRecoveryQuestion(rs.getString("RECOVERYQUESTION"));
		u.setRecoveryAnswer(rs.getString("RECOVERYANSWER"));
		u.setCanPost(rs.getBoolean("CANPOST"));
		u.setCanComment(rs.getBoolean("CANCOMMENT"));
		u.setEmailVerified(rs.getBoolean("EMAILVERIFIED"));
		return u;
	}
	
}
