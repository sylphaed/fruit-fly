package net.riftwalkinto1v3.signup;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class SignUpForm extends ActionForm {

	private static final long serialVersionUID = -4312139148379076636L;
	private String name;
	private String password;
	private String email;
	private Date birthday;
	private String recoveryQuestion;
	private String recoveryAnswer;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getRecoveryQuestion() {
		return recoveryQuestion;
	}
	public void setRecoveryQuestion(String recoveryQuestion) {
		this.recoveryQuestion = recoveryQuestion;
	}
	public String getRecoveryAnswer() {
		return recoveryAnswer;
	}
	public void setRecoveryAnswer(String recoveryAnswer) {
		this.recoveryAnswer = recoveryAnswer;
	}
	
	@Override
	public ActionErrors validate(ActionMapping mapping,
	HttpServletRequest request) {
 
	   ActionErrors errors = new ActionErrors();
 
	    if( getName() == null || ("".equals(getName()))) {
	       errors.add("common.name.err",
                         new ActionMessage("error.common.name.required"));
	    }
 
	    return errors;
	}
 
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// reset properties
		name = "";
	}
	
}
