package net.riftwalkinto1v3.signup;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.riftwalkinto1v3.User;
import net.riftwalkinto1v3.UserDAO;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SignUpAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		try (Connection connection = DriverManager
				.getConnection("jdbc:sqlite:testdb.db");) {
			
			UserDAO userDAO = new UserDAO(connection);
			SignUpForm signUpForm = (SignUpForm) form;
			User user = new User(signUpForm.getName(), signUpForm.getPassword(), signUpForm.getEmail());
			
			
			userDAO.storeUser(user);
			
			return mapping.findForward("success");
		}
	}
	


}
