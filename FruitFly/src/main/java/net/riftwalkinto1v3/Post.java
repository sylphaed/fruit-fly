package net.riftwalkinto1v3;
import java.util.Date;

public class Post {
	
	
	public Post(String author, String title, String body) {
		//new post with date timestamped to the moment it was created
		this.date = new java.sql.Date(new Date().getTime());
		this.author = author;
		this.title = title;
		this.body = body;
	}
	public Post(Date date, String author, String title, String body) {
		this.date = date;
		this.author = author;
		this.title = title;
		this.body = body;
	}
	
	private Date date;
	private String author;
	private String title;
	private String body;
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setDate(long time){
		this.date.setTime(time);
	}
	public String getTitle() {
		return title;
	}
	public String getTitleURL(){
		return title.replaceAll(" ", "-").toLowerCase();
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
}
