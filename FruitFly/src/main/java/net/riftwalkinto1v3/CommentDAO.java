package net.riftwalkinto1v3;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
public class CommentDAO {

	public CommentDAO(Connection connection) {
		this.connection = connection;
	}
	
	private final Connection connection;

	public void storeComment(Comment c) throws SQLException{
		try(
				Statement statement = connection.createStatement();
				PreparedStatement ps = connection.prepareStatement("INSERT "
						+ "INTO COMMENTS(DATE, POSTKEY, AUTHOR, BODY) VALUES(?, ?, ?, ?)");
			){
			ps.setDate(1, new java.sql.Date(c.getDate().getTime()));
			ps.setDate(2, new java.sql.Date(c.getPostKey().getTime()));
			ps.setString(3, c.getAuthor());
			ps.setString(4, c.getBody());			
		}
	}
	
	public ArrayList<Comment> getCommentsOnPost(Date postKey)
			throws SQLException {
		ArrayList<Comment> list = new ArrayList<>();
		try (Statement statement = connection.createStatement();
				PreparedStatement ps = connection
						.prepareStatement("SELECT * FROM COMMENTS "
								+ "WHERE POSTKEY = ? ORDER BY DATE DESC")) {
			ps.setDate(1, new java.sql.Date(postKey.getTime()));
			try(ResultSet rs = ps.executeQuery()){
				while (rs.next())
					list.add(buildComment(rs));
			}
		}

		return list;
	}

	public Comment getCommentByID(int ID) throws SQLException {
		Comment c = null;
		try (Statement statement = connection.createStatement();
				PreparedStatement ps = connection
						.prepareStatement("SELECT * FROM COMMENTS WHERE ID = ?")) {
			ps.setInt(1, ID);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next())
					c = buildComment(rs);
				else{
					//TODO if a comment query comes up empty
				}
			}
		}

		return c;
	}

	private Comment buildComment(ResultSet rs) throws SQLException {
		return new Comment(rs.getDate("DATE"), rs.getDate("POSTKEY"), 
				rs.getString("AUTHOR"), rs.getString("BODY"));
	}
}
